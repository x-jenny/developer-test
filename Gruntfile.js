module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Minify js files
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        files: [
          {src: 'source/js/App.js', dest: 'dist/js/app.min.js'},
          {src: 'source/js/App2.js', dest: 'dist/js/app2.min.js'},     
        ]
      }
    },

    // Minify CSS files
    cssmin: {
      target: {
        files: {
          'dist/css/app.min.css': "source/css/app.css"
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Default task(s).
  grunt.registerTask('minify', ['uglify', 'cssmin']);

};