function onEnter(event, obj) {
    if (event.keyCode === 13) {
        search(obj.id);
    }
}

function search(inputid) {
    $.ajax({
        url: "products.php",
        data: $("#" + inputid).serialize(),
        success: function(data) {
                     displayResult(data);
                 },
        error: function(xhr) {
            removeOldContent();
            var errorText = "Error: " + xhr.status + " - " + xhr.statusText;
            var errorDiv = addDiv(errorText, "page_width result matches");
            document.body.appendChild(errorDiv);
        }
    });
}

function displayResult(data) {
    removeOldContent();
    var sumText = data.length + " matches";
    var sumDiv = addDiv(sumText, "page_width result matches");
    document.body.appendChild(sumDiv);
    for (i = 0; i < data.length; i++) {
        var wrapperDiv = addDiv("", "page_width result top_margin");
        var serviceDiv = addDiv("Service", "left_col");
        var snameContent = '<a href="' + data[i].url + '">' + data[i].name + '</a>';
        var snameDiv = addDiv(snameContent, "")
        wrapperDiv.appendChild(serviceDiv);
        wrapperDiv.appendChild(snameDiv);
        var gwrapperDiv = addDiv("", "page_width result");
        var groupDiv = addDiv("Group", "left_col");
        var gnameDiv = addDiv(data[i].group, "");
        gwrapperDiv.appendChild(groupDiv);
        gwrapperDiv.appendChild(gnameDiv);
        document.body.appendChild(wrapperDiv);
        document.body.appendChild(gwrapperDiv);
    } 
}

function addDiv(content, classAttr) {
    var div = document.createElement("div");
    if (content) {
        div.innerHTML = content;
    }
    if (classAttr) {
        div.setAttribute("class", classAttr);
    }
    return div;
}

function removeOldContent() {
    var oldContent = $(".result");
    for (i = 0; i < oldContent.length; i++) {
        oldContent[i].parentNode.removeChild(oldContent[i]);
    }
}