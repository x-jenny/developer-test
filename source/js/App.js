function search(formid, tableid) {
    $.ajax({
        url: "products.php",
        data: $("#" + formid).serialize(),
        success: function(data) {
                     displayResult(data, tableid);
                 }
    });
}

function displayResult(data, tableid) {
    var new_tbody = document.createElement('tbody');
    for (i = 0; i < data.length; i++) {
        row = new_tbody.insertRow(i);
        row.insertCell(0).innerHTML = '<a href="' + data[i].url + '">' + data[i].name + '</a>';
        row.insertCell(1).innerHTML = data[i].group;
    }
    var old_tbody = document.getElementById(tableid).getElementsByTagName('tbody')[0];
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
}