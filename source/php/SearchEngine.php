<?php

namespace KnowitToolsTest;

class SearchEngine
{
    /** @var array */
    private $data = array();

    /** @var array */
    private $keywords = array();

    /** @var array */
    private $sortkeys = array();

    /** @var array */
    private $result = array();
    
    /**
     * @param array  $data     Dataset
     * @param string $query    Search query
     * @param array  $sortkeys Keys to sort the result by
     */
    public function __construct ($data, $query, $sortkeys)
    {
        $this->data = $data;
        $this->keywords = explode(" ", $query);
        $this->sortkeys = $sortkeys;
    }

    /**
     * Filters the dataset by keywords
     */
    public function filter()
    {
        foreach ($this->keywords as $keyword) {
            if ($keyword[0] === '-') {
                $this->removeMatches($keyword);
            }
            else {
                $this->addMatches($keyword);
            }
        }
        $this->sortByRelevance();
    }

    public function getResult()
    {
        return $this->result;
    }

    /**
     * Sorts the result in the order the keys are given
     */
    private function sortByKeys()
    {
        foreach ($this->sortkeys as $key) {
            if (array_key_exists($key, $this->result[0])) {
                $arrays[] = array_column($this->result, $key);
            }
        }

        $arrays[] = &$this->result;
        array_multisort(...$arrays);
    }

    private function sortByRelevance()
    {
        $points = array();
        $keywords = $this->nonNegativeKeywords();
        $keywords = array_map("strtolower", $keywords);
        foreach ($this->result as $index => $item) {
            $title = explode(" ", strtolower($item['name']));
            $points[] = count(array_diff($keywords, $title));
        }
        array_multisort($points, $this->result);
    }

    /**
     * Adds entries matching the keyword to search result
     */
    private function addMatches($keyword)
    {
        foreach ($this->data as $index => $item) {
            if (stripos($item["name"], $keyword) !== false) {
                $this->result[] = $item;
                unset($this->data[$index]);
            }
        }
    }

    /**
     * Removes entries matching the keyword from search result
     */
    private function removeMatches($keyword)
    {
        $keyword = substr($keyword, 1);
        foreach ($this->result as $index => $item) {
            if (stripos($item["name"], $keyword) !== false) {
                unset($this->result[$index]);
            }
        }
        // Re-indexes $this->result that has been altered by unset()
        $this->result = array_values($this->result);
    }

    private function nonNegativeKeywords()
    {
        foreach ($this->keywords as $keyword) {
            if ($keyword[0] !== '-') {
                $keywords[] = $keyword;
            }
        }

        return $keywords ? $keywords : array();
    }
}